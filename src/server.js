import sirv from "sirv";
import polka from "polka";
import compression from "compression";
import * as sapper from "@sapper/server";
import { json } from "body-parser";
import fetch from "node-fetch";
import session from "express-session";
import sessionFileStore from "session-file-store";
import { getCookies } from "./routes/api/discord";
const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === "development";

const FileStore = new sessionFileStore(session);

polka() // You can also use Express
  .use(
    json(),
    session({
      secret: process.env.POLKA_PRIVATEKEY,
      resave: true,
      saveUninitialized: true,
      store: new FileStore({
        path: `.sessions`,
      }),
    }),
    compression({ threshold: 0 }),
    sirv("static", { dev }),
    sapper.middleware({
      session: async (req, res) => {
        let discordAuth = {};
        const discordCookie = getCookies(req.headers.cookie);
        const request = await fetch(`https://discordapp.com/api/users/@me`, {
          headers: {
            Authorization: `Bearer ${discordCookie.discord_access_token}`,
          },
        });
        let results = await request.json();
        if (results.id) {
          discordAuth = results;
        }
        return { token: req.session.token, discordUser: discordAuth };
      },
    })
  )
  .listen(PORT, (err) => {
    if (err) console.error("error", err);
  });
