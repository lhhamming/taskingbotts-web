import * as db from "mysql2";

const dotenv = require("dotenv").config();
const bcrypt = require("bcrypt");
const net = require("net");
const pool = createPool();

export async function register(username, password) {
  const userExists = await query("SELECT * FROM user WHERE web_name = ?", [
    username,
  ]);
  if (userExists || userExists.length < 1) {
    const hash = bcrypt.hashSync(password, 10);
    await query("INSERT INTO user (web_name, web_password) VALUES (?,?)", [
      username,
      hash,
    ]);
    return true;
  } else {
    return false;
  }
}

export async function registerDiscord(
  discord_username,
  discord_id,
  discord_tag,
  discord_token
) {
  const userExists = await query("SELECT * FROM user WHERE discord_id = ?", [
    discord_id,
  ]);
  if (userExists.length < 1) {
    await query(
      "INSERT INTO user (discord_name, discord_id, discord_tag ,discord_token) VALUES (?,?,?,?)",
      [discord_username, discord_id, discord_tag, discord_token]
    );
  }
}

export async function registerDiscordNoToken(
  discord_username,
  discord_id,
  discord_tag
) {
  const userExists = await query("SELECT * FROM user WHERE discord_id = ?", [
    discord_id,
  ]);
  if (userExists.length < 1) {
    await query(
      "INSERT INTO user (discord_name, discord_id, discord_tag) VALUES (?,?,?)",
      [discord_username, discord_id, discord_tag]
    );
    return true;
  }
  return false;
}

export async function login(username, password) {
  try {
    const userExists = await query("SELECT * FROM user WHERE web_name = ?", [
      username,
    ]);
    if (userExists) {
      const userPassword = userExists.results.shift().web_password;
      const valid = await bcrypt.compare(password, userPassword);
      if (valid) {
        return true;
      }
    }
    return false;
  } catch (error) {
    console.error("mysql.js:login", error);
  }
}

//User actions

export async function getUsers() {
  const users = await query("SELECT * FROM user");
  return users;
}

export async function getUser(id) {
  const user = await query("SELECT * FROM user WHERE id = ?", [id]);
  return user;
}

export async function getDiscordUser(id) {
  const user = await query("SELECT * FROM user WHERE discord_id = ?", [id]);
  return user;
}

export async function insertDiscordUser(
  discord_id,
  discord_name,
  discord_tag
) {}

//Role actions

export async function getRoles() {
  const roles = await query("SELECT * FROM role");
  return roles;
}

export async function updateRole(uId, role_id) {
  const updateRole = await query("UPDATE user SET role_id = ? WHERE id = ?", [
    role_id,
    uId,
  ]);
  return updateRole;
}

//Commands action

export async function addCommand(name, description, cooldown) {
  const addCommand = await query(
    "INSERT INTO command (name,description,cooldown) VALUES (?,?,?)",
    [name, description, cooldown]
  );
  return addCommand;
}

export async function getCommands() {
  const commands = await query("SELECT * FROM command");
  return commands;
}

export async function getCommand(commandName) {
  const command = await query("SELECT * FROM command WHERE name = ?", [
    commandName,
  ]);
  return command;
}

//Cooldown requests

export async function addCooldown(discordId, commandName) {
  const command = await getCommand(commandName);
  const user = await getDiscordUser(discordId);
  const commandCooldown = command[0].cooldown * 1000 + Date.now();
  await query(
    "INSERT INTO cooldown (cooldown ,user_id, command_id) VALUES (?,?,?)",
    [commandCooldown, user[0].id, command[0].id]
  );
}

export async function getCooldown(discordId, commandName) {
  const command = await getCommand(commandName);
  // TODO: Make web checking. only discord id's will work now.
  const user = await getDiscordUser(discordId);
  const cooldown = await query(
    "SELECT * FROM cooldown WHERE user_id = ? AND command_id = ?",
    [user[0].id, command[0].id]
  );
  return cooldown;
}

export async function setCooldown(discordId, commandName) {
  const command = await getCommand(commandName);
  // TODO: Make web checking. only discord id's will work now.
  const user = await getDiscordUser(discordId);
  const commandCooldown = command[0].cooldown * 1000 + Date.now();
  await query(
    "UPDATE cooldown SET cooldown = ? WHERE user_id = ? AND command_id = ?",
    [commandCooldown, user[0].id, command[0].id]
  );
}

// Discord bot requests

export async function getPrefix(guildId) {
  const prefix = await query("SELECT prefix FROM prefix WHERE guild_id = ?", [
    guildId,
  ]);
  return prefix;
}

// Item requests

export async function getItems() {
  const items = await query("SELECT * FROM item");
  return items;
}

export async function getItem(itemName){
  const item = await query("SELECT * FROM item WHERE name = ?", 
  [itemName]
  )
  return item
}

export async function addItem(name,value,craftable,maxstack,tradeable,obtainable,examine){
  const itemExist = await getItem(name);
  if(itemExist.length > 0){
    return ({error: "Item already exists"})
  }
  const insertItem = await query("INSERT INTO item (name,value,craftable,maxstack,tradeable,obtainable,examine) VALUES (?,?,?,?,?,?,?)",
  [name,value,craftable,maxstack,tradeable,obtainable,examine]
  )
  return insertItem;
}

// drop tables

export async function getDroptables(){
  const droptables = await query("SELECT * FROM droptable");
  return droptables
}

export async function getDroptable(name){
  const droptable = await query("SELECT * FROM droptable WHERE name = ?", 
  [name]
  )
  return droptable
}

export async function addDroptable(name){
  const droptableExists = await getDroptable(name)
  if(droptableExists.length > 0){
    return ({error: "Droptable exists"})
  }else{
    await query("INSERT INTO droptable (name) VALUES (?)", 
    [name]
    )
    return ({success: "Droptable added to the database!"})
  }
}

// query handling

async function query(sql, values) {
  const [rows, fields, error] = await pool.query(sql, values);
  if (error) {
    console.error("Error in query:\t", error);
  } else {
    return rows;
  }
}

function createPool() {
  try {
    const pool = db.createPool({
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      waitForConnections: true,
      stream: function (opts) {
        const socket = net.connect(opts.config.port, opts.config.host);
        socket.setKeepAlive(true);
        return socket;
      },
      connectionLimit: 10,
      queueLimit: 0,
      connectTimeout: 28800,
    });

    const promisePool = pool.promise();
    return promisePool;
  } catch (error) {
    console.error(error);
  }
}
