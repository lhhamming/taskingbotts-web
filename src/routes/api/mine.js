import * as dbActions from "../../mysql/mysql";
import { hasCooldown } from "./cooldown";

const commandName = "mine";

export async function get(req, res, next) {
  const discordId = req.query.discordId;
  const userExists = await dbActions.getDiscordUser(discordId);
  if (userExists.length < 1) {
    res.end(JSON.stringify({ notRegistered: "User is not registerd!" }));
  }
  const hasToWait = await hasCooldown(discordId, commandName);
  if (hasToWait.cooldown) {
    res.end(
      JSON.stringify({
        cooldown: "command is cooldown!",
        embed: [
          {
            name: "Cooldown!",
            value: `You still have to wait \`${hasToWait.cooldown}\` seconds!`,
          },
        ],
      })
    );
  }

  res.end(
    JSON.stringify({
      embed: [
        { name: "Mine", value: "You swing your pickaxe from side to side" },
      ],
      color: "#d9d4ce",
    })
  );
}
