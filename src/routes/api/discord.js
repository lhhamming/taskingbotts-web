const dotenv = require("dotenv").config();
export async function post(req, res, next) {}

export async function get(req, res, next) {
  const cookies = getCookies(req.headers.cookie);
  if (!cookies.disco_refresh_token) {
    res.end(JSON.stringify({ error: "No discord refresh token" }));
  }
  if (cookies.disco_refresh_token && !cookies.discord_access_token) {
    //token expired
    const response = refreshToken(cookies.disco_refresh_token);
    const access_token_expires_in = new Date(Date.now() + response.expires_in);
    const refresh_token_expires_in = new Date(
      Date.now() + 30 * 24 * 60 * 60 * 1000
    );
    res.writeHead(200, {
      "Set-Cookie": [
        `discord_access_token=${discordResponse.access_token}; Path=/; HttpOnly; SameSite=Strict; Expires=${access_token_expires_in}}`,
        `disco_refresh_token=${discordResponse.refresh_token}; Path=/; HttpOnly; SameSite=Strict; Expires=${refresh_token_expires_in}`,
      ],
    });
    res.end(JSON.stringify({ success: "Token refreshed!" }));
  }
}

async function refreshToken(refreshToken) {
  const dataObject = {
    client_id: process.env.BOT_ID,
    client_secret: process.env.BOT_SECRET,
    grant_type: "refresh_token",
    redirect_uri: process.env.REDIRECT_URI,
    refresh_token: refreshToken,
    scope: "identify email guilds",
  };

  const request = await fetch("https://discord.com/api/oauth2/token", {
    method: "POST",
    body: new URLSearchParams(dataObject),
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
  });

  const response = await request.json();

  if (response.error) {
    console.error(response.error);
    return JSON.stringify({
      error: "Could not refresh discord token! Please login again.",
    });
  }
  if (response) {
    return response;
  }
}

export function getCookies(reqCookies) {
  if (reqCookies) {
    let cookies = reqCookies.split("; ").map((cookie) => {
      return cookie.split("=");
    });
    cookies = Object.fromEntries(cookies);
    return cookies;
  }
  return [];
}
