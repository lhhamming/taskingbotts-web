import * as dbActions from '../../mysql/mysql'

export async function get(req,res,next){
    const droptables = await dbActions.getDroptables();
    res.end(JSON.stringify({result: droptables}))
}

export async function post(req,res,next){
    const addDroptable = await dbActions.addDroptable(req.body.dropTableName);
    res.end(JSON.stringify({result : addDroptable}))
}