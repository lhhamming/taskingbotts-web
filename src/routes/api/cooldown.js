import * as dbActions from "../../mysql/mysql";

export async function get(req, res, next) {
  // ! check if the user exists in the database! otherwise register it.
  const discordId = req.query.discordId;
  await dbActions.getUser(discordId);
}

export async function post(req, res, next) {
  // ? We need: executing principal ( discord user id ) & what command ( command id ).
  // Cooldown amount can be fetched by getting the command.
  const commandName = req.body.commandName;
  const discordId = req.body.id;
  const command = dbActions.getCommand(commandName);
}

export async function hasCooldown(discordId, commandName) {
  const currentUserCooldown = await dbActions.getCooldown(
    discordId,
    commandName
  );
  if (currentUserCooldown.length < 1) {
    await dbActions.addCooldown(discordId, commandName);
    return { result: false };
  } else if (Date.now() > currentUserCooldown[0].cooldown) {
    await dbActions.setCooldown(discordId, commandName);
    return { result: false };
  } else {
    return {
      cooldown: (currentUserCooldown[0].cooldown - Date.now()) / 1000,
      result: true,
    };
  }
}
