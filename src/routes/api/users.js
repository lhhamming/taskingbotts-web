import * as dbActions from "../../mysql/mysql";

export async function get(req, res, next) {
  let users;
  if (req.query.id !== undefined) {
    users = await dbActions.getUser(req.query.id);
  } else if (req.query.discordId !== undefined) {
    users = await dbActions.getDiscordUser(req.query.discordId);
  } else {
    users = await dbActions.getUsers();
  }
  res.end(JSON.stringify({ result: users }));
}

export async function put(req, res, next) {
  const userInfo = req.body.userInfo;
}
