import { getPrefix } from "../../mysql/mysql";

export async function get(req, res, next) {
  const guildId = req.query.guildId;
  const fetchedPrefix = await getPrefix(guildId);
  res.end(JSON.stringify({ result: fetchedPrefix }));
}
