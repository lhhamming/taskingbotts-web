import * as dbActions from "../../mysql/mysql";

export async function get(req, res, next) {
  const roles = await dbActions.getRoles();
  res.end(JSON.stringify({ result: roles }));
}

export async function put(req, res, next) {
  const userId = req.body.userId;
  const roleId = req.body.roleId;
  const roleUpdate = await dbActions.updateRole(userId, roleId);
  res.end(JSON.stringify({ result: roleUpdate }));
}
