import * as dbActions from "../../mysql/mysql";

export async function get(req, res, next) {
  if (req.query.command) {
    const fetchedCommand = await dbActions.getCommand(req.query.command);
    res.end(JSON.stringify({ command: fetchedCommand }));
  } else {
    const commands = await dbActions.getCommands();
    res.end(JSON.stringify({ list: commands }));
  }
}

export async function post(req, res, next) {
  const commandName = req.body.commandName;
  const commandDesc = req.body.commandDesc;
  const commandCooldown = req.body.commandCooldown;
  if (commandName && commandDesc && commandCooldown !== undefined) {
    dbActions.addCommand(
      commandName.toLowerCase(),
      commandDesc,
      commandCooldown
    );
    res.end(JSON.stringify({ success: "added command!" }));
  } else {
    console.error("Command name: ", commandName);
    console.error("Command description: ", commandDesc);
    console.error("Command cooldown: ", commandCooldown);
    res.end(
      JSON.stringify({
        error: "A paramter has not been fulfilled or isnt correct!",
      })
    );
  }
}
