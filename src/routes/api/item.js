import * as dbAction from "../../mysql/mysql";

export async function get(req, res, next) {
    const items = await dbAction.getItems();
    res.end(JSON.stringify({ result: items }));
}

export async function post(req,res,next){
    const itemAddResult = await dbAction.addItem(req.body.name, req.body.value, req.body.craftable, req.body.maxstack, req.body.tradeable, req.body.obtainable, req.body.examine)
    res.end(JSON.stringify({result: itemAddResult}))
}
