export async function put(req, res, next) {
  try {
    req.session.token = undefined;
    res.end(JSON.stringify({ token: undefined }));
  } catch (error) {
    console.error(error);
  }
}
