const jwt = require("jsonwebtoken");
const dotenv = require("dotenv").config();

import * as dbActions from "../mysql/mysql";

export async function post(req, res, next) {
  try {
    if (req.body.discordId) {
      const success = await dbActions.registerDiscordNoToken(
        req.body.discordName,
        req.body.discordId,
        req.body.discordTag
      );
      if (success) {
        res.end(
          JSON.stringify({
            embed: [
              {
                name: "Register success!",
                value: "You can now use taskingbotts with its full potential",
              },
            ],
            color: "#67c722",
          })
        );
      } else {
        res.end(
          JSON.stringify({
            embed: [
              { name: "Register failed!", value: "You are already registerd!" },
            ],
            color: "#c72222",
          })
        );
      }
    }
    const username = req.body.username;
    const password = req.body.password;
    const result = await dbActions.register(username, password);
    if (result) {
      const token = sign(username);
      req.session.token = token;
      res.end(JSON.stringify({ token: token }));
    }
  } catch (error) {
    console.error("register.js:error", error);
    res.end(JSON.stringify({ error: error }));
  }
}

function sign(username) {
  return jwt.sign(`Bearer ${username}`, process.env.JWT_PRIVATEKEY, {
    algorithm: "HS256",
  });
}
