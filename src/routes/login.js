const fetch = require("node-fetch");
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv").config();

import * as dbActions from "../mysql/mysql";

export async function post(req, res, next) {
  try {
    if (req.body.code) {
      const code = req.body.code;
      const dataObject = {
        client_id: process.env.BOT_ID,
        client_secret: process.env.BOT_SECRET,
        grant_type: "authorization_code",
        redirect_uri: process.env.REDIRECT_URI,
        code: code,
        scope: "identify email guilds",
      };

      const discordRequest = await fetch(
        "https://discord.com/api/oauth2/token",
        {
          method: "POST",
          body: new URLSearchParams(dataObject),
          headers: { "Content-Type": "application/x-www-form-urlencoded" },
        }
      );

      const discordResponse = await discordRequest.json();
      const discordUserRequest = await fetch(
        `https://discordapp.com/api/users/@me`,
        {
          headers: {
            Authorization: `${discordResponse.token_type} ${discordResponse.access_token}`,
          },
        }
      );
      const discordUserResponse = await discordUserRequest.json();

      if (discordUserResponse.id) {
        dbActions.registerDiscord(
          discordUserResponse.username,
          discordUserResponse.id,
          discordUserResponse.discriminator,
          discordResponse.refresh_token
        );
        const access_token_expires_in = new Date(
          Date.now() + discordUserResponse.expires_in
        );
        const refresh_token_expires_in = new Date(
          Date.now() + 30 * 24 * 60 * 60 * 1000
        );
        res.writeHead(200, {
          "Set-Cookie": [
            `discord_access_token=${discordResponse.access_token}; Path=/; HttpOnly; SameSite=Strict; Expires=${access_token_expires_in}}`,
            `disco_refresh_token=${discordResponse.refresh_token}; Path=/; HttpOnly; SameSite=Strict; Expires=${refresh_token_expires_in}`,
          ],
        });
        res.end(JSON.stringify({ success: "Discord account registered!" }));
      }
    } else {
      const username = req.body.username;
      const password = req.body.password;
      const result = await dbActions.login(username, password);
      if (result) {
        const token = sign(username);
        req.session.token = token;
        res.end(JSON.stringify({ token: token }));
      }
    }
  } catch (error) {
    console.error("login.js:error", error);
    res.end(JSON.stringify({ error: error }));
  }
}

export async function get(req, res, next) {
  let cookies = req.headers.cookie.split("; ").map((cookie) => {
    return cookie.split("=");
  });
  cookies = Object.fromEntries(cookies);
  res.end(JSON.stringify({ token: cookies.discord_access_token }));
}

function sign(username) {
  return jwt.sign(`Bearer ${username}`, process.env.JWT_PRIVATEKEY, {
    algorithm: "HS256",
  });
}
