# TaskingBotTs-Web

A webclient made for my TaskingBot that is written in Typescript (Hence the name TaskingBotTs). This webclient will:
Handle requests for the bot (restful API)
Host its own version of the bot made in the Sapper / Svelte framework. 